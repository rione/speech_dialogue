#!/usr/bin/env python
# -*- coding: utf-8 -*-
 
import rospy
import math
import treetaggerwrapper
import os
from std_msgs.msg import String
from std_msgs.msg import Int32

dict = {}
#csvファイルから質問と解答を読み込み辞書型としてdictに保存する
def readQuestion():
    with open(os.environ['HOME']+"/catkin_ws/src/speech_dialogue/data/q_en.csv","r") as f:
        line2 = f.readlines()
        for line in line2:
            line = line.rstrip()
            line = unicode(line,'utf_8')
            line3 = line.split(";")
            dict[line3[0]] = line3[1]
 
#形態素解析結果から自立語のみを抜き出す
def getNouns(message):
    list = [u'NN', u'NNS', u'NP', u'NPS', u'PP', u'PP$', u'JJ', u'VV', u'WRB', u'WP', u'WDT']
    tagger = treetaggerwrapper.TreeTagger(TAGLANG='en',TAGDIR=os.environ['HOME']+'/tree-tagger')
    tags = tagger.TagText(message)
    noun = []
    for tag in tags:
        if tag.split('\t')[1] in list:
            noun.append(tag.split('\t')[0])
    return noun
    
#内積を返す    
def dotProduct(listX, listY):
    sum = 0
    for noun in listX:
        if noun in listY:
            sum += 1
    return sum
    
#ノルムを返す    
def getSqrt(listX):
    return math.sqrt(len(listX))
    
#2つのベクトルのコサイン類似度を返す    
def getCosSimilarity(nouns1, nouns2):
    upper = dotProduct(nouns1,nouns2)
    bottom = getSqrt(nouns1) * getSqrt(nouns2)
    return float(upper)/bottom
    
#dictからもっともコサイン類似度の高い質問を探し、それに対応する解答を返す    
def getAnswer(message, dict):
    answer = None
    nouns1 = getNouns(message)
    if len(nouns1) == 0:
        return answer
    max = 0
    for k,v in dict.items():
        nouns2 = getNouns(k)
        score = getCosSimilarity(nouns1,nouns2)
        if max < score:
            max = score
            answer = v
    return answer

#answerをresponseトピックにpubする                
def callback(data):
    message = unicode(data.data,'utf-8')
    text = getAnswer(message,dict)
    if text:
        if text.isdigit():
            num = int(text)
            pub = rospy.Publisher('operate', Int32, queue_size=10)
            pub.publish(num)
        else:
            pub = rospy.Publisher('response', String, queue_size=10)
            pub.publish(text)
 
def run():
    rospy.init_node('answer_node', anonymous=True)
    rospy.Subscriber('question', String, callback)
    rospy.spin()
 
if __name__ == '__main__':
    try:
        readQuestion()
        run()
    except rospy.ROSInterruptException:
        pass
