#!/usr/bin/env python
# -*- coding: utf-8 -*-
 
import rospy
import sys
import subprocess
import time
import os
from std_msgs.msg import String
from std_msgs.msg import Int32

ar4 = 0

def getObjectName():

    # 2秒待つ
    time.sleep(2)

    '''
    :param cmd: str 実行するコマンド.
    :rtype: str
    :return: 標準出力.
    '''

    arg1 = "cfg/yolo.cfg"
    arg2 = "yolo.weights"
    arg3 = os.environ['HOME']+"/image.jpg"
    global arg4
    command = "sh "+os.environ['HOME']+"/catkin_ws/src/speech_dialogue/src/execute_darknet.sh " + arg1 + " " + arg2 + " " + arg3 + str(arg4)
    arg4 += 1

    # ここでプロセスが (非同期に) 開始する.
    proc = subprocess.Popen(command, shell=True,
                            stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
    buf = []

    while True:
        # バッファから1行読み込む.
        line = proc.stdout.readline()
        buf.append(line)
        sys.stdout.write(line)
        # バッファが空 + プロセス終了.
        if not line and proc.poll() is not None:
            break

    # darknetの出力結果を格納
    result_str = ''.join(buf)
    # オブジェクト名（カテゴリ名）を格納
    word_list = []
    word = ""
    itis = "It is "
    cateis = "This category is "

    # 後ろ向きにループ
    for i in xrange(1, 100):
        if(result_str[-i] != "\n"):
            word += result_str[-i]
        if(len(word) > 1 and (result_str[-i] == "%" or result_str[-i] == ".")):
            word_list.append(word[0:-1][::-1])
            word = result_str[-i]
        if(word == "."):
            break

    # 結果が分からなかった時
    if(word_list[0].find(":") == -1):
        word_list = ["unknown: 100%"]

    # csv読み込み
    dict = {}
    # with open(os.environ['HOME'] +
    # "/catkin_ws/src/speech_dialogue/data/object.csv", "r") as f:

    with open(os.environ['HOME'] + "/catkin_ws/src/speech_dialogue/data/object.csv", "r") as f:
        line2 = f.readlines()
        for line in line2:
            line = line.rstrip()
            line = unicode(line, 'utf_8')
            line3 = line.split(",")
            dict[line3[0]] = line3[1]

    # print(dict)

    # word_sorted -> [[word, percent(int)]]
    word_sorted = []
    max_per_word = []

    for i in xrange(len(word_list)):
        max_per_word.append([word_list[i].split(":")[0], int(
            word_list[i].split(":")[1].split("%")[0])])
        max_per_word = sorted(max_per_word, key=lambda x: x[1])
    max_per_word = max_per_word[-1][0]
    # print(max_per_word)
    # sys.exit()

    for i in xrange(len(word_list)):
        word_sorted.append([word_list[i].split(":")[0]])
    word_sorted.sort()
    # print(word_sorted)

    dark_cat = ""
    for i in xrange(len(word_sorted)):
        dark_cat = dark_cat + word_sorted[i][0]

    print(dark_cat)

    word = dict.get(dark_cat, None)

    if(word is None):
        return itis + max_per_word + "."
    else:
        return itis + word.split(":")[0] + "." + cateis + word.split(":")[1] + "."

    return itis + max_per_word + "."

def callback(data):
    if data.data == 5:
        text = getObjectName()
        if text:
            print text
            pub = rospy.Publisher('response', String, queue_size=10)
            pub.publish(text)
         
def run():
    rospy.init_node('darknet_node', anonymous=True)
    rospy.Subscriber('operate', Int32, callback)
    rospy.spin()
 
if __name__ == '__main__':
    try:
        run()
    except rospy.ROSInterruptException:
        pass
