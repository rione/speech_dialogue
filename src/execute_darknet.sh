#!/bin/bash

# 実行時に指定された引数の数、つまり変数 $# の値が 4 でなければエラー終了。
if [ $# -ne 4 ]; then
  echo "need 3 argument!!" 1>&2
  exit 1
fi

# ./darknet detect cfg/yolo.cfg yolo.weights data/dog.jpg

cd ~/darknet
./darknet detect $1 $2 $3
mv predictions.png ~/$4.jpg

# ./darknet detect cfg/yolo.cfg yolo.weights data/dog.jpg
