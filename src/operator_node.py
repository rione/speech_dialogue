#!/usr/bin/env python
# -*- coding: utf-8 -*-
 
import rospy
import re
from std_msgs.msg import String
from std_msgs.msg import Int32

#operatorを取得する
def getOperator(flag):
    a = flag.group('operation')
    if a == 'stop':
        return 0
    elif a == 'follow me':
        return 1
    elif a == 'kitchen':
        return 2
    elif a == 'car':
        return 3
    elif a == 'living room':
	    return 4	

def callback(data):
    flag = re.compile('(?P<operation>stop|follow me|kitchen|car|living room)').search(data.data) 
    if flag:
        n = getOperator(flag)
        print n
        pub = rospy.Publisher('operate', Int32, queue_size=10)
        pub.publish(n)
         
def run():
    rospy.init_node('operator_node', anonymous=True)
    rospy.Subscriber('operator', String, callback)
    rospy.spin()
 
if __name__ == '__main__':
    try:
        run()
    except rospy.ROSInterruptException:
        pass
