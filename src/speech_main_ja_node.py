#!/usr/bin/env python
# -*- coding: utf-8 -*-
 
import rospy

from rospeex_if import ROSpeexInterface
from std_msgs.msg import String

class speech(object):
    def __init__(self):
        self._interface = None
        
    def callback(self, data):
        text = unicode(data.data, 'utf-8')
        print text
        self._interface.say(text, 'ja', 'nict')
        
    def sr_response(self, message):
        if message:
            message ='ここは' + message + 'ですね、覚えました'
            print message
            self._interface.say(message, 'ja', 'nict')
 
    def run(self):
        rospy.init_node('speech_main_ja_node', anonymous=True)
        rospy.Subscriber('response', String, self.callback)
        self._interface = ROSpeexInterface()
        self._interface.init()
        self._interface.register_sr_response(self.sr_response)
        self._interface.set_spi_config(language='ja', engine='nict')
        rospy.spin()
 
if __name__ == '__main__':
    try:
        node = speech()
        node.run()
    except rospy.ROSInterruptException:
        pass
