#!/usr/bin/env python
# -*- coding: utf-8 -*-
 
import rospy

from rospeex_if import ROSpeexInterface
from std_msgs.msg import String

class speech(object):
    def __init__(self):
        self._interface = None
        
    #メッセージの型を決めてから要編集   
    def callback(self, data):
        text = unicode(data.data, 'utf-8')
        print text
        self._interface.say(text, 'en', 'nict')
    	    
    def isOperator(self, message):
        if 'please' in message or 'Please' in message:
            return True
        else:
            return False
        
    def sr_response(self, message):
        if message:
            print message
            if self.isOperator(message):
                pub_o = rospy.Publisher('operator', String, queue_size=10)
                pub_o.publish(message)
            else:
                pub_q = rospy.Publisher('question', String, queue_size=10)
                pub_q.publish(message)
 
    def run(self):
        rospy.init_node('speech_main_node', anonymous=True)
        rospy.Subscriber('response', String, self.callback)
        self._interface = ROSpeexInterface()
        self._interface.init()
        self._interface.register_sr_response(self.sr_response)
        self._interface.set_spi_config(language='en', engine='nict')
        rospy.spin()
 
if __name__ == '__main__':
    try:
        node = speech()
        node.run()
    except rospy.ROSInterruptException:
        pass
